package com.mvnproject.sample;

import static org.junit.Assert.assertNotEquals;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testGreeting() {
    	App app = new App();
    	assertNotNull(app.getGreeting());
    }
    public void testAddition() {
    	App app = new App();
    	assertEquals(10, app.add(5, 5));
    }
}
